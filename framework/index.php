<?php
// Require All Backend Dependencies
$dependencies_backend = array(
    'slideshow-plugin/backend/slideshow-setting.php',
    'slideshow-plugin/backend/slideshow-actions.php',
    'includes/backend/enqueue/enqueue.php',
    'contributor-plugin/backend/contributor-metabox.php'
);

foreach ($dependencies_backend as $dependency_backend):
    include $dependency_backend;
endforeach;

// Require All Frontend Dependencies
$dependencies_frontend = array(
    'includes/frontend/enqueue/enqueue.php',
    'slideshow-plugin/frontend/slideshow-shortcode.php',
    'contributor-plugin/frontend/content-filter.php'
);

foreach ($dependencies_frontend as $dependency_frontend):
    include $dependency_frontend;
endforeach;
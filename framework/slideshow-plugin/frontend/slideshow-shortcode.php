<?php
function mitchdesigns_slideshow_shortcode()
{
    $slideshow_images = get_option('slideshow_image_list');
    if (!empty($slideshow_images)) {

        $content = '<script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("#imageGallery").lightSlider({
                    gallery: true,
                    item: 1,
                    thumbItem: 10,
                    slideMargin: 0,
                    currentPagerPosition: "left",
                });
            });
        </script>';
        $content .= '<ul id="imageGallery">';
        foreach ($slideshow_images as $image_id) {
            $image_thumbnail_obj = wp_get_attachment_image_src($image_id, 'thumbnail');
            $image_thumbnail_src = $image_thumbnail_obj[0];
            $image_medium_obj = wp_get_attachment_image_src($image_id, 'medium');
            $image_medium_src = $image_medium_obj[0];
            $image_large_obj = wp_get_attachment_image_src($image_id, 'md-slideshow');
            $image_large_src = $image_large_obj[0];


            $content .= ' <li data-thumb="' . $image_thumbnail_src . '" data-src="' . $image_medium_src . '"><img src="' . $image_large_src . '"/></li>';

        }

        $content .= '</ul>';

    }
    return $content;

}

add_shortcode('mdslideshow', 'mitchdesigns_slideshow_shortcode');
?>
<?php
function custom_content_after_post($content)
{
    if (is_single()) {
        $post_id = get_the_ID();
        $contributors_list = get_post_meta($post_id, '_post_multi_contributors', true);
        $post_author_id = get_the_author_meta('ID');
        $post_author_profile = get_author_posts_url($post_author_id);
        $post_author_avatar = get_avatar($post_author_id, 50);
        $content .= '<div class="author-area">';
        $content .= '<h5>Author</h5>';
        $content .= $post_author_avatar;
        $content .= '<a href="' . $post_author_profile . '">' . get_the_author() . '</a>';
        $content .= '</div>';
        if (!empty($contributors_list)) {
            $content .= '<h5>Contributors</h5>';
            $content .= '<ul class="contributors-list">';
            foreach ($contributors_list as $contributor) {
                $contributor_info = get_userdata($contributor);
                $contributor_id = $contributor_info->ID;
                $contributor_name = $contributor_info->display_name;
                $contributor_profile = get_author_posts_url($contributor_id);
                $contributor_avatar = get_avatar($contributor_id, 32);
                $content .= '<li>' . $contributor_avatar . '<a href="' . $contributor_profile . '">' . $contributor_name . '</a>';
            }
            $content .= '</ul>';
        }
    }
    return $content;
}

add_filter('the_content', 'custom_content_after_post');

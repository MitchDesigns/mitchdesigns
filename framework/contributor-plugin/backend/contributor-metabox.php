<?php

// Create Contributors Meta Box
function post_contributors_meta_box()
{
    if (current_user_can('delete_published_posts')) {
        add_meta_box('post_contributors', 'Post Contributors', 'post_contributors_meta_box_callback', 'post');
    }

}

add_action('add_meta_boxes', 'post_contributors_meta_box');

function post_contributors_meta_box_callback($post)
{

    wp_nonce_field('post_contributors_meta_box', 'post_contributors_meta_box_nonce');

    echo '<label>';
    echo 'Select Authors';
    echo '</label>';

    // Get Current User Data
    $current_user_info = wp_get_current_user();
    $current_user_id = $current_user_info->ID;

    // Meta Value
    $value = get_post_meta($post->ID, '_post_multi_contributors', true);

    // Query Contributors
    $args = array(
        'role' => 'Author',
    );
    $contributor_query = new WP_User_Query($args);
    if (!empty($contributor_query->results)) {
        echo '<ul class="horizontal">';
        foreach ($contributor_query->results as $contributor) {
            if ($contributor->ID != $current_user_id) {
                echo '<li><label><input type="checkbox" name="postContributorsChecklist[]" value="' . $contributor->ID . '" ' . (in_array($contributor->ID, $value) ? 'checked' : '') . ' />' . $contributor->display_name . '</label></li>';
            }
        }
        echo '</ul>';
    } else {
        echo 'No users found.';
    }

}

function post_contributors_save_meta_box_data($post_id)
{

    if (!isset($_POST['post_contributors_meta_box_nonce'])) {
        return;
    }

    if (!wp_verify_nonce($_POST['post_contributors_meta_box_nonce'], 'post_contributors_meta_box')) {
        return;
    }

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    // Check the user's permissions.
    if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {

        if (!current_user_can('edit_page', $post_id)) {
            return;
        }

    } else {

        if (!current_user_can('edit_post', $post_id)) {
            return;
        }
    }

    if (!isset($_POST['postContributorsChecklist'])) {
        return;
    }

    $my_data = $_POST['postContributorsChecklist'];

    update_post_meta($post_id, '_post_multi_contributors', $my_data);
}

add_action('save_post', 'post_contributors_save_meta_box_data');
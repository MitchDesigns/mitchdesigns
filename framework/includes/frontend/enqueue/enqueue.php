<?php
/*
 * Scripts and Styles enqueue Frontend
 */
function frontend_custom_enqueue()
{
    //Register Style and Script
    wp_register_style('light_slider_css', get_template_directory_uri() . '/css/lightSlider.css');
    wp_register_script('light_slider_js', get_template_directory_uri() . '/js/jquery.lightSlider.js', array('jquery'), '1.0', false);

    //Enqueue Styles and Scripts
    wp_enqueue_style('light_slider_css');
    wp_enqueue_script('light_slider_js');

}

add_action('wp_enqueue_scripts', 'frontend_custom_enqueue');
jQuery('document').ready(function ($) {
    $('#mdSlideshowUpload').on('click', function () {
        var image_id_array = new Array();
        $('#mdSlideshowImagesList li').each(function () {
            image_id_array.push($(this).data('imageid'));
        });
        var upload_box = wp.media({
            title: 'Upload Photos',
            button: {
                text: 'Upload'
            },
            multiple: true,
            type: 'image'
        }).open();
        upload_box.on('select', function () {
            var image_list = upload_box.state().get('selection').toJSON();
            var attachments_number = image_list.length;
            var uploaded_images = new Array();
            for (i = 0; i < attachments_number; i++) {
                image_id_array.push('' + image_list[i].id + '');
                uploaded_images.push('' + image_list[i].id + '');
            }
            console.log(image_id_array);
            $.ajax({
                type: 'POST',
                url: wp_ajax.ajaxurl,
                data: {
                    action: 'upload_slideshow_images_action',
                    image_id_array: image_id_array,
                    uploaded_images: uploaded_images
                },
                success: function (data, textStatus, XMLHttpRequest) {
                    var data = $.parseJSON(data);
                    if (data.error) {
                        console.log(data.error.msg);
                    } else {
                        $('#mdSlideshowImagesList').append(data.success.new_images);

                    }
                }
            });
        });
    });

    $('#mdSlideshowSave').on('click', function () {
        var current_button = $(this);
        current_button.html('<i class="fa fa-refresh fa-spin"></i>');
        var image_id_array = new Array();
        $('#mdSlideshowImagesList li').each(function () {
            image_id_array.push($(this).data('imageid'));
        });
        console.log(image_id_array);
        $.ajax({
            type: 'POST',
            url: wp_ajax.ajaxurl,
            data: {
                action: 'save_slideshow_images_changes_action',
                image_id_array: image_id_array
            },
            success: function (data, textStatus, XMLHttpRequest) {
                var data = $.parseJSON(data);
                if (data.error) {
                    console.log(data.error.msg);
                } else {
                    console.log(data.success.msg);
                    current_button.html('Save Changes');

                }
            }
        });
    });

    $('.md-slideshow-remove').on('click', function () {
        $(this).parents('li').fadeOut('slow', function () {
            $(this).remove();
            var image_id_array = new Array();
            $('#mdSlideshowImagesList li').each(function () {
                image_id_array.push($(this).data('imageid'));
            });
            console.log(image_id_array);
            $.ajax({
                type: 'POST',
                url: wp_ajax.ajaxurl,
                data: {
                    action: 'save_slideshow_images_changes_action',
                    image_id_array: image_id_array
                },
                success: function (data, textStatus, XMLHttpRequest) {
                    var data = $.parseJSON(data);
                    if (data.error) {
                        console.log(data.error.msg);
                    } else {
                        console.log(data.success.msg);

                    }
                }
            });
        });

    });
});